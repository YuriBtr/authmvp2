package yuribtr.skillbranch.ru.mvpauth2.mvp.models;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth2.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;

public class CatalogModel {
    DataManager mDataManager = DataManager.getInstance();

    public CatalogModel() {
    }

    public List<ProductDto> getProductList() {
        return mDataManager.getMockProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }
}
