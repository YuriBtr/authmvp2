package yuribtr.skillbranch.ru.mvpauth2.mvp.presenters;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth2.mvp.models.CatalogModel;
import yuribtr.skillbranch.ru.mvpauth2.mvp.views.ICatalogView;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter{
    private static CatalogPresenter ourInstance = new CatalogPresenter();
    private CatalogModel mCatalogModel;
    private List<ProductDto> mProductDtoList;

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mCatalogModel = new CatalogModel();
    }

    @Override
    public void initView() {
        if (mProductDtoList == null) {
            mProductDtoList = mCatalogModel.getProductList();
        }
        if (getView() != null){
            getView().showCatalogView(mProductDtoList);
        }
    }

    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().showAddToCartMessage(mProductDtoList.get(position));
                getView().updateProductCounter();
            } else {
                getView().showAuthScreen();
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }
}
