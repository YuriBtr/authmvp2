package yuribtr.skillbranch.ru.mvpauth2.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
