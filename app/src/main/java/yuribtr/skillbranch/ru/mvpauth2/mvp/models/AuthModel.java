package yuribtr.skillbranch.ru.mvpauth2.mvp.models;

import android.content.Context;
import android.os.AsyncTask;

import yuribtr.skillbranch.ru.mvpauth2.R;
import yuribtr.skillbranch.ru.mvpauth2.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth2.mvp.presenters.AuthPresenter;
import yuribtr.skillbranch.ru.mvpauth2.utils.NetworkStatusChecker;

public class AuthModel implements IAuthModel{
    private Context mContext;
    private DataManager mDataManager;

    public AuthModel() {
        mDataManager = DataManager.getInstance();
        mContext = mDataManager.getContext();
    }

    public boolean isAuthUser(){
        return mDataManager.isAuthUser();
    }

    private void saveAuthToken(String authToken){
        mDataManager.saveAuthToken(authToken);
    }

    public void loginUser (String email, String password) {
        if (isNetworkPresence()) {

            class WaitSplash extends AsyncTask<Void, Void, Void> {
                protected Void doInBackground(Void... params) {
                    try {
                        Thread.currentThread();
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
                protected void onPostExecute(Void result) {
                    super.onPostExecute(result);
                    saveAuthToken("authenticated");
                    AuthPresenter.getInstance().onLoginSuccess();
                }
            }
            WaitSplash waitSplash = new WaitSplash();
            waitSplash.execute();
        }
        else
            AuthPresenter.getInstance().onLoginError(DataManager.getInstance().getContext().getString(R.string.error_network_failure));
    }

    private boolean isNetworkPresence(){
        return NetworkStatusChecker.isNetworkAvailable(mContext);
    }
}
