package yuribtr.skillbranch.ru.mvpauth2.mvp.presenters;

public interface IProductPresenter {
    void clickOnPlus();
    void clickOnMinus();
}
