package yuribtr.skillbranch.ru.mvpauth2.mvp.views;

import java.util.List;

import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;

public interface ICatalogView extends IView {
    void showAddToCartMessage(ProductDto productDto);
    void showCatalogView(List<ProductDto> productsList);
    void showAuthScreen();
    void updateProductCounter();
}
