package yuribtr.skillbranch.ru.mvpauth2.mvp.models;

public interface IAuthModel {

    boolean isAuthUser();
    void loginUser (String email, String password);

}
