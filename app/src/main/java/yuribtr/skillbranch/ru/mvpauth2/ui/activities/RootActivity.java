package yuribtr.skillbranch.ru.mvpauth2.ui.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import yuribtr.skillbranch.ru.mvpauth2.BuildConfig;
import yuribtr.skillbranch.ru.mvpauth2.R;
import yuribtr.skillbranch.ru.mvpauth2.mvp.views.IFragmentManagerView;
import yuribtr.skillbranch.ru.mvpauth2.ui.fragments.AccountFragment;
import yuribtr.skillbranch.ru.mvpauth2.ui.fragments.AuthFragment;
import yuribtr.skillbranch.ru.mvpauth2.ui.fragments.CatalogFragment;
import yuribtr.skillbranch.ru.mvpauth2.utils.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth2.utils.TransformRoundedImage;

public class RootActivity extends BaseActivity implements IFragmentManagerView, NavigationView.OnNavigationItemSelectedListener {
    //так как у этой активити пока нет презентера, хранить счетчик негде, поэтому и статическая переменная (позже будет переделано, с появлением презентера)
    private static int sGoodsCounter = 0;
    Menu mMenu;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorContainer;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    ImageView mUserAvatar;
    ImageView mCartBadgeIcon;
    TextView mCartBadgeText;

    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        initToolbar();
        initDrawer();

        mUserAvatar = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.user_avatar);
        Picasso.with(this)
                .load(R.drawable.user_avatar)//load user photo via presenter/PM here
                .placeholder(R.drawable.user_avatar)
                .error(R.drawable.user_avatar)
                .fit()
                .centerCrop()
                .transform(new TransformRoundedImage())
                .into(mUserAvatar);

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null)
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }


    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    public void setCartQuantity(int quantity){
        sGoodsCounter = sGoodsCounter + quantity;
    }

    public void updateCartQuantity(){
        if (sGoodsCounter>0) {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_black_24dp);
            mCartBadgeText.setText(String.valueOf(sGoodsCounter));
            mCartBadgeText.setVisibility(View.VISIBLE);
        } else {
            mCartBadgeIcon.setImageResource(R.drawable.ic_shopping_cart_grey_24dp);
            mCartBadgeText.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.badge_menu, mMenu);
        MenuItem item = mMenu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.cart_badge);
        LinearLayout relativeLayout = (LinearLayout) MenuItemCompat.getActionView(item);
        mCartBadgeIcon = (ImageView) relativeLayout.findViewById(R.id.cart_icon_iv);
        mCartBadgeText = (TextView) relativeLayout.findViewById(R.id.cart_count_tv);
        setCartQuantity(0);
        updateCartQuantity();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START))
            mDrawer.closeDrawer(GravityCompat.START);
        else if (mFragmentManager.getBackStackEntryCount() > 0) {mFragmentManager.popBackStack();}
        else showDialog(ConstantManager.CHECK_EXIT);
        //moveTaskToBack(true);
    }


    protected Dialog onCreateDialog(int id) {
        if (id == ConstantManager.CHECK_EXIT) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.check_exit_title);
            builder.setMessage(R.string.check_exit_question);
            builder.setIcon(android.R.drawable.ic_dialog_info);
            builder.setPositiveButton(R.string.yes, myClickListener);
            builder.setNegativeButton(R.string.no, myClickListener);
            return builder.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    moveTaskToBack(true);
                    break;
                case Dialog.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                item.setChecked(true);
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                item.setChecked(true);
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notification:
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public void showAuthFragment(){
        Fragment fragment = new AuthFragment();
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void goToPrevFragment(){
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            mFragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    //region================IView================
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.unknown_error));
            //todo:send error stacktrace to crashlytics
        }

    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }
    //endregion
}