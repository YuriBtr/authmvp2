package yuribtr.skillbranch.ru.mvpauth2.mvp.views;

import android.content.Context;
import android.support.annotation.Nullable;

import yuribtr.skillbranch.ru.mvpauth2.mvp.presenters.IAuthPresenter;
import yuribtr.skillbranch.ru.mvpauth2.ui.custom_views.AuthPanel;

public interface IAuthView extends IView{

    IAuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    String getEmail();
    String getPassword();

    void setNonAcceptableEmail();
    void setNonAcceptablePassword();

    void setAcceptableEmail();
    void setAcceptablePassword();

    void setWrongEmailError();
    void setWrongPasswordError();

    void removeWrongEmailError();
    void removeWrongPasswordError();

    Context getContext();

    void showLoginProgress();
    void hideLoginProgress();

    void showCatalogScreen();

    @Nullable
    AuthPanel getAuthPanel();
}
