package yuribtr.skillbranch.ru.mvpauth2.mvp.models;

import yuribtr.skillbranch.ru.mvpauth2.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;

public class ProductModel {
    DataManager mDataManager = DataManager.getInstance();

    public ProductDto getProductById(int productId){
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }
}
