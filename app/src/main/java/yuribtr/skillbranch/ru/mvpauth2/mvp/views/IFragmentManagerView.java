package yuribtr.skillbranch.ru.mvpauth2.mvp.views;

public interface IFragmentManagerView extends IView{
    void goToPrevFragment();
}

