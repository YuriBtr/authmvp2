package yuribtr.skillbranch.ru.mvpauth2.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import yuribtr.skillbranch.ru.mvpauth2.R;
import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth2.mvp.presenters.CatalogPresenter;
import yuribtr.skillbranch.ru.mvpauth2.mvp.views.ICatalogView;
import yuribtr.skillbranch.ru.mvpauth2.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth2.ui.fragments.adapters.CatalogAdapter;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener{
    private static final String TAG = "CatalogFragment";
    private CatalogPresenter mPresenter = CatalogPresenter.getInstance();
    @BindView(R.id.add_to_card_btn)
    Button mAddToCartBtn;
    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.indicator)
    CircleIndicator mCircleIndicator;

    public CatalogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        mPresenter.takeView(this);
        mPresenter.initView();
        mAddToCartBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_to_card_btn) {
            mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
        }
    }

    //region================ICatalogView================
    @Override
    public void showAddToCartMessage(ProductDto productDto) {
        getRootActivity().setCartQuantity(productDto.getCount());
        showMessage(getString(R.string.goods)+productDto.getProductName()+getString(R.string.successfully_added_to_cart));
    }

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product: productsList){
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
        mCircleIndicator.setViewPager(mProductPager);
    }

    @Override
    public void showAuthScreen() {
        getRootActivity().showAuthFragment();
    }

    @Override
    public void updateProductCounter() {
        getRootActivity().updateCartQuantity();
    }
    //endregion

    ////region================IView================
    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }
}
