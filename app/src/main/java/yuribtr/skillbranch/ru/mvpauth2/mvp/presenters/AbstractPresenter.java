package yuribtr.skillbranch.ru.mvpauth2.mvp.presenters;

import android.support.annotation.Nullable;

import yuribtr.skillbranch.ru.mvpauth2.mvp.views.IView;

public abstract class AbstractPresenter<T extends IView> {
    private T mView;

    public void takeView(T view){
        mView = view;
    }

    public void dropView() {
        mView = null;
    }

    public abstract void initView() ;

    @Nullable
    public T getView() {
        return mView;
    }

}
