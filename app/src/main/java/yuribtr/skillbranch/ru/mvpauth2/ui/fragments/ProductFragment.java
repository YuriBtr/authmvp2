package yuribtr.skillbranch.ru.mvpauth2.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.Locale;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import yuribtr.skillbranch.ru.mvpauth2.R;
import yuribtr.skillbranch.ru.mvpauth2.data.storage.dto.ProductDto;
import yuribtr.skillbranch.ru.mvpauth2.mvp.presenters.ProductPresenter;
import yuribtr.skillbranch.ru.mvpauth2.mvp.presenters.ProductPresenterFactory;
import yuribtr.skillbranch.ru.mvpauth2.mvp.views.IProductView;
import yuribtr.skillbranch.ru.mvpauth2.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth2.ui.custom_views.AspectRatioImageView;
import yuribtr.skillbranch.ru.mvpauth2.utils.ConstantManager;

//we have to extend Fragment from support library
public class ProductFragment extends Fragment implements IProductView, View.OnClickListener{
    private static final String TAG = "ProductFragment";
    @BindView(R.id.product_name_tv)
    TextView mProductNameTxt;
    @BindView(R.id.product_description_tv)
    TextView mProductDescriptionTxt;
    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;
    @BindView(R.id.product_count_tv)
    TextView mProductCountTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;
    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @BindDrawable(R.drawable.radio_image)
    Drawable mProductDraw;

    private ProductPresenter mPresenter;

    public ProductFragment() {
    }

    public static ProductFragment newInstance (ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ConstantManager.PRODUCT_KEY, product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle){
        if (bundle != null){
            ProductDto product = bundle.getParcelable(ConstantManager.PRODUCT_KEY);
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //region================IProductView================
    @Override
    public void showProductView(ProductDto product) {
        mProductNameTxt.setText(product.getProductName());
        mProductDescriptionTxt.setText(product.getDescription());
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount()>0)
            mProductPriceTxt.setText(adoptPrice(product.getCount()*product.getPrice()));
        else
            mProductPriceTxt.setText(adoptPrice(product.getPrice()));

        //Picasso.with(mProductImage.getContext()).setLoggingEnabled(true);
        Picasso.with(mProductImage.getContext()).setIndicatorsEnabled(true);
        DisplayMetrics metrics = new DisplayMetrics();
        getRootActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width, height;
        if (metrics.widthPixels<metrics.heightPixels) {
            width = (int) (metrics.widthPixels / 2f);
            height = (int) (width/AspectRatioImageView.getDefaultAspectRatio());
        } else {
            height = (int) (metrics.heightPixels / 5f);
            width = (int) (height*AspectRatioImageView.getDefaultAspectRatio());
        }

        Picasso.with(mProductImage.getContext())
                .load(product.getImageUrl())
                .resize(width, height)
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .centerCrop()
                .into(mProductImage);
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount()>0)
            mProductPriceTxt.setText(adoptPrice(product.getCount()*product.getPrice()));
        else
            mProductPriceTxt.setText(adoptPrice(product.getPrice()));
    }

    private String adoptPrice(int price){
        return "$"+String.format(new Locale("ru"), "%,d", price)+".-";
    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
        }

    }
}
